<?php

    trait Hewan {
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi(){
            echo $this->nama . ' sedang ' . $this->keahlian;
        }

    }    
    
    abstract class Fight{
        use Hewan;

        public $attackPower;
        public $defencePower;

        public function serang($hewan){
            echo $this->nama . ' sedang menyerang ' . $hewan->nama;
            echo '<br>';

            // method diserang otomatis dipanggil jika method serang dipanggil
            $hewan->diserang($this);
        }

        public function diserang($hewan){
            echo $this->nama . ' sedang diserang ' . $hewan->nama;

            $this->darah = $this->darah - $hewan->attackPower / $this->defencePower;;
        }

        protected function getInfo(){
            echo 'Nama : ' . $this->nama . '<br>';
            echo 'Jumlah Kaki : ' . $this->jumlahKaki . '<br>';
            echo 'Keahlian : ' . $this->keahlian . '<br>';
            echo 'Darah : ' . $this->darah . '<br>';
            echo 'Attack Power : ' . $this->attackPower . '<br>';
            echo 'Defence Power : ' . $this->defencePower;
        }
        abstract public function getInfoHewan();

    }

    class Elang extends Fight{

        public function __construct($nama){
            $this->nama = $nama;
            
            $this->jumlahKaki = 2;
            $this->keahlian = 'terbang tinggi';
            $this->attackPower = 10;
            $this->defencePower = 5;
        }

        public function getInfoHewan(){
            echo 'Jenis Hewan : Elang <br>';
            $this->getInfo();
        }

    }

    class Harimau extends Fight{

        public function __construct($nama){
            $this->nama = $nama;

            $this->jumlahKaki = 4;
            $this->keahlian = 'lari cepat';
            $this->attackPower = 7;
            $this->defencePower = 8;
        }

        public function getInfoHewan(){
            echo 'Jenis Hewan : Harimau <br>';
            $this->getInfo();
        }

    }

class Spasi {
    public static function jeda(){
        echo '<br>';
        echo '========';
        echo '<br><br>';
    }
}

$harimau = new Harimau('Harimau Sumatera');
$harimau->getInfoHewan();
echo '<br> Atraksi: ';
$harimau->atraksi();
Spasi::jeda();
$elang = new Elang('Elang Gurun');
$elang->getInfoHewan();
echo '<br> Atraksi: ';
$elang->atraksi();
Spasi::jeda();
$harimau->serang($elang);
Spasi::jeda();
$elang->getInfoHewan();
Spasi::jeda();
$elang->serang($harimau);
Spasi::jeda();
$harimau->getInfoHewan()
?>
