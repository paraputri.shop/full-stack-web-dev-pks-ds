<?php

namespace App;

use App\Roles;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use illuminate\Foundation\Auth\Users as Authenticatable;

class Users extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = ['username', 'email', 'name', 'role_id','password', 'email_verified_at'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->role_id = Roles::where('name' , 'author')->first()->id;
        }); 
        
    }

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode','user_id');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
